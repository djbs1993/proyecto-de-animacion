using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    public float rotationSpeed;
    public Vector3 rotationAxis;
    void Update ()
    {
        transform.Rotate (rotationAxis * (rotationSpeed * Time.deltaTime));
    }

   
}